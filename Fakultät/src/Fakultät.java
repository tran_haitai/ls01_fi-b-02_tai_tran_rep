
public class Fakultät {
	
	public static int faculty(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n*faculty(n-1);
		}
	}
	
	public static String facultyString(int n) {
		if (n == 0) {
			return "";
		} else if (n == 1) {
			return "1";
		} else {
			String str = "1";
			for (int i = 2; i <= n; i++) {
				str += " * " + Integer.toString(i);
			}
			return str;
		}
	}
	
	public static void printFaculty(int n) {
		if (n <= 5) {
			for (int i = 0; i <= n; i++) {
				System.out.printf("%d%-4s= %-18s=%4s%n", i, "!", facultyString(i), faculty(i));
			}
		} else {
			for (int i = 0; i <= n; i++) {
				System.out.printf("%d%s = %s = %s%n", i, "!", facultyString(i), faculty(i));
			}
		}
	}

	public static void main(String[] args) {
		
		System.out.printf("%-5s=%-19s=%4s%n", "0!", "", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "1!", " 1", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "2!", " 1 * 2", "2");
		System.out.printf("%-5s=%-19s=%4s%n", "3!", " 1 * 2 * 3", "6");
		System.out.printf("%-5s=%-19s=%4s%n", "4!", " 1 * 2 * 3 * 4", "24");
		System.out.printf("%-5s=%-19s=%4s%n", "5!", " 1 * 2 * 3 * 4 * 5", "120");
		
		System.out.println();
		System.out.println();
		
		printFaculty(5);

	}

}
