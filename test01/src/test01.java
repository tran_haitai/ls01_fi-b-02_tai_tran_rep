
public class test01 {
	
	public static int methode(String s) {
		int l = s.length();
		int i = 0;
		int sum = 0;
		
		while (i < l) {
			sum += s.charAt(i);
			i++;
		}
		
		return sum;
	}

	public static void main(String[] args) {
		System.out.println(methode("a"));

	}

}
