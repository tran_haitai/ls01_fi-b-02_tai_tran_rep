﻿import java.util.Arrays;
import java.util.Scanner;

class Fahrkartenautomat {
	private static String[] fahrkarten = {
			"Einzelfahrschein Berlin AB",
			"Einzelfahrschein Berlin BC",
			"Einzelfahrschein Berlin ABC",
			"Kurzstrecke",
			"Tageskarte Berlin AB",
			"Tageskarte Berlin BC",
			"Tageskarte Berlin ABC",
			"Kleingruppen-Tageskarte Berlin AB",
			"Kleingruppen-Tageskarte Berlin BC",
			"Kleingruppen-Tageskarte Berlin ABC",
	};
	private static double[] fahrkartenPreise = {
			2.90,
			3.30,
			3.60,
			1.90,
			8.60,
			9.00,
			9.60,
			23.50,
			24.30,
			24.90,
	};
	
	private static double[] geldMoeglich = {
			2,
			1,
			0.5,
			0.2,
			0.1,
			0.05,
			0.02,
			0.01,
	};
	
	private static int[] kasse = {
			10,
			10,
			10,
			10,
			10,
			10,
			10,
			10,		
	};
	
	private static int[] tmpKasse = {};
	
	private static int rohlinge = 10;
	
	private static int tmpRohlinge = 0;
	
	private static void kasseAnzeigen() {
		String[] geldString = {"2 EURO", "1 EURO", "50 CENT", "20 CENT", "10 CENT", "5 CENT", "2 CENT", "1 CENT"};
		System.out.println("Kasseninhalt:");
		for (int i = 0; i < kasse.length; i++) {
			System.out.printf("  %8s -- %2d Stück.%n", geldString[i], kasse[i]);
		}
		System.out.println();
	}
	
	private static void kasseBefuellen() {
		for (int i = 0; i < kasse.length; i++) {
			kasse[i] = 20;
		}
		System.out.println("--Kasse befüllt--");
	}
	
	private static void kasseLeeren() {
		for (int i = 0; i < kasse.length; i++) {
			kasse[i] = 0;
		}
		System.out.println("--Kasse geleert--");
	}
	
	private static void inKasseEinwerfen(double geld) {
		int i = 7;
		int geldInCent = euroZuCent(geld);
		switch (geldInCent) {
		case 200:
			i = 0;
			break;
		case 100:
			i = 1;
			break;
		case 50:
			i = 2;
			break;
		case 20:
			i = 3;
			break;
		case 10:
			i = 4;
			break;
		case 5:
			i = 5;
			break;
		case 2:
			i = 6;
			break;
		case 1:
			i = 7;
			break;
		}
		
		if (kasse[i] < 20) {
			kasse[i] += 1;
		}
	}
	
	private static void vonKasseEntfernen(int[] rueckgeldZaehler) {
		for (int i = 0; i < rueckgeldZaehler.length; i++) {
			kasse[i] -= rueckgeldZaehler[i];
		}
	}
	
	private static void rohlingeAnzeigen() {
		System.out.println("Vorhandene Ticket-Rohlinge: " + rohlinge);
		System.out.println();
	}
	
	private static void rohlingeWechseln() {
		rohlinge = 10;
		System.out.println("--Ticket-Rohlinge gewechselt--");
		System.out.println();
	}
	
	private static boolean arrayEnthaeltDouble(double[] arr, double value) {
    	boolean enthalten = false;
    	for (int i = 0; i < arr.length; i++) {
    		if (arr[i] == value) {
    			enthalten = true;
    			break;
    		}
    	}
    	return enthalten;
    }
	
	private static int[] arrayHinzufuegen(int[] array, int wert) {
	    int[] laengeresArray = new int[array.length + 1];
	    for (int i = 0; i < array.length; i++)
	    	laengeresArray[i] = array[i];
	    laengeresArray[array.length] = wert;
	    return laengeresArray;
	}
    
    private static double strZuDouble(String str) {
    	return Double.parseDouble(str.replace(",", "."));
    }
    
    private static int euroZuCent(double wert) {
		return (int)Math.round(wert*100);
	}
    
    private static String[] muenzeZuString(int wert) {
		String[] arr = {"", ""};
		switch(wert) {
			case 200:
				arr[0] = "2";
				arr[1] = "EURO";
				break;
			case 100:
				arr[0] = "1";
				arr[1] = "EURO";
				break;
			case 50:
				arr[0] = "50";
				arr[1] = "CENT";
				break;
			case 20:
				arr[0] = "20";
				arr[1] = "CENT";
				break;
			case 10:
				arr[0] = "10";
				arr[1] = "CENT";
				break;
			case 5:
				arr[0] = "5";
				arr[1] = "CENT";
				break;
			case 2:
				arr[0] = "2";
				arr[1] = "CENT";
				break;
			case 1:
				arr[0] = "1";
				arr[1] = "CENT";
				break;
		}
		return arr;
	}
    
    private static int[] rueckgeldZaehlen(int[] array) {
    	int[] tmpArray = {
    			0,
    			0,
    			0,
    			0,
    			0,
    			0,
    			0,
    			0,
    	};
    	
    	for (int i = 0; i < array.length; i++) {
			switch (array[i]) {
				case 200:
					tmpArray[0] += 1;
					break;
				case 100:
					tmpArray[1] += 1;
					break;
				case 50:
					tmpArray[2] += 1;
					break;
				case 20:
					tmpArray[3] += 1;
					break;
				case 10:
					tmpArray[4] += 1;
					break;
				case 5:
					tmpArray[5] += 1;
					break;
				case 2:
					tmpArray[6] += 1;
					break;
				case 1:
					tmpArray[7] += 1;
					break;
			}
		}
    	
    	return tmpArray;
    }
    
    private static void printMuenzen(int anzahl, int[] wert) {		
		if (anzahl == 3) {
			String[] werte1 = muenzeZuString(wert[0]);
			String[] werte2 = muenzeZuString(wert[1]);
			String[] werte3 = muenzeZuString(wert[2]);
			
			System.out.println("   * * *       * * *       * * *   ");
			System.out.println(" *       *   *       *   *       * ");
			System.out.printf("*    %-2s   * *    %-2s   * *    %-2s   *", werte1[0], werte2[0], werte3[0]);
			System.out.printf("%n*   %s  * *   %s  * *   %s  *%n", werte1[1], werte2[1], werte3[1]);
			System.out.println(" *       *   *       *   *       * ");
			System.out.println("   * * *       * * *       * * *   ");
		} else if (anzahl == 2) {
			String[] werte1 = muenzeZuString(wert[0]);
			String[] werte2 = muenzeZuString(wert[1]);
			
			System.out.println("   * * *       * * *   ");
			System.out.println(" *       *   *       * ");
			System.out.printf("*    %-2s   * *    %-2s   *", werte1[0], werte2[0]);
			System.out.printf("%n*   %s  * *   %s  *%n", werte1[1], werte2[1]);
			System.out.println(" *       *   *       * ");
			System.out.println("   * * *       * * *   ");
		} else {
			String[] werte1 = muenzeZuString(wert[0]);
			
			System.out.println("   * * *   ");
			System.out.println(" *       * ");
			System.out.printf("*    %-2s   *", werte1[0]);
			System.out.printf("%n*   %s  *%n", werte1[1]);
			System.out.println(" *       * ");
			System.out.println("   * * *   ");
		}
	}
	
	private static void muenzeAusgeben(int[] rueckgeld) {
		while (rueckgeld.length > 0) {
			if (rueckgeld.length > 2) {
				int[] tmpArray = {rueckgeld[0], rueckgeld[1], rueckgeld[2]};
				printMuenzen(3, tmpArray);
				rueckgeld = Arrays.copyOfRange(rueckgeld, 3, rueckgeld.length);
			} else if (rueckgeld.length == 2) {
				int[] tmpArray = {rueckgeld[0], rueckgeld[1]};
				printMuenzen(2, tmpArray);
				rueckgeld = Arrays.copyOfRange(rueckgeld, 2, rueckgeld.length);
			} else {
				int[] tmpArray = {rueckgeld[0]};
				printMuenzen(1, tmpArray);
				rueckgeld = Arrays.copyOfRange(rueckgeld, 1, rueckgeld.length);
			}
		}
	}
	
	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag;
		int anzahl;
		int fahrkartenWahl;
		tmpKasse = new int[0];
		
		while (true) {
			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
			for (int i = 1; i <= fahrkarten.length; i++) {
				System.out.printf("%n %40s  %6.2f€  (%d)", fahrkarten[i-1], fahrkartenPreise[i-1], i);
			}
			
			System.out.printf("%n%n");
			System.out.print("Ihre Wahl: ");
			
			try {
				fahrkartenWahl = tastatur.nextInt();
			} catch (Exception e) {
				tastatur.next();
				fahrkartenWahl = 0;
			}
		    
		    if (fahrkartenWahl > 0 && fahrkartenWahl <= fahrkarten.length) {
		    	break;
		    } else {
		    	System.out.println("Bitte wählen sie unter den gegebenen Fahrkarten aus!");
		    }
		}
		
		System.out.println("Fahrkarte: " + fahrkarten[fahrkartenWahl-1]);
	    
	    zuZahlenderBetrag = fahrkartenPreise[fahrkartenWahl-1];
	    
	    while (true) {
	    	System.out.print("Anzahl der Tickets (1-10): ");

		    try {
		    	anzahl = tastatur.nextInt();
		    } catch (Exception e) {
		    	tastatur.next();
		    	anzahl = 0;
		    }
		    
		    if (anzahl > 0 && anzahl < 11) {
		    	break;
		    } else {
		    	System.out.println("Die Anzahl der Tickets darf minimal 1 und maximal 10 betragen!");
		    }
	    }
	    
	    zuZahlenderBetrag *= anzahl;
	    tmpRohlinge = anzahl;

	    return zuZahlenderBetrag;
	}
	
	private static void warte(int millisekunde) {
		int waitTime = (int) Math.round(millisekunde/10);
		for (int i = 0; i < 10; i++) {
	    	System.out.print("=");
	    	try {
	    		Thread.sleep(waitTime);
	    	} catch (InterruptedException e) {
	    		e.printStackTrace();
			}
	    }
	}
	
	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	    warte(2000);
	}
	
	private static double fahrkartenBezahlen(double zuZahlen, Scanner tastatur) {
		double eingeworfenesGeld;
		double eingezahlterGesamtbetrag = 0.0;
		
		while(eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Geld-Eingabe (mind. 1Ct, höchstens 2 Euro): ");
			
			try {
				eingeworfenesGeld = strZuDouble(tastatur.next());
			} catch (Exception e) {
				eingeworfenesGeld = 0;
			}
			
			if (arrayEnthaeltDouble(geldMoeglich, eingeworfenesGeld)) {
				inKasseEinwerfen(eingeworfenesGeld);
				tmpKasse = arrayHinzufuegen(tmpKasse, euroZuCent(eingeworfenesGeld));
				eingezahlterGesamtbetrag += eingeworfenesGeld;
			} else {
				System.out.println("Geldeinwurf ist nicht zulässig!");
			}
		}
		fahrkartenAusgeben();
		return eingezahlterGesamtbetrag - zuZahlen;
	}
	
	private static void rueckgeldAusgeben(double rueckgabebetrag) {
		int[] rueckgeld = {};
		int[] kassenCheck = Arrays.copyOfRange(kasse, 0, kasse.length);
		int rueckgabebetragInCentGesamt = 0;
		
		if(rueckgabebetrag > 0.0) {
	    	int rueckgabebetragInCent = euroZuCent(rueckgabebetrag);
	    	rueckgabebetragInCentGesamt = rueckgabebetragInCent;
	    	
	        while(rueckgabebetragInCent >= 200 && kassenCheck[0] > 0) // 2 EURO-Muenzen
	        	{
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 200);
		        	rueckgabebetragInCent -= 200;
		        	kassenCheck[0] -= 1;
	            }
	        while(rueckgabebetragInCent >= 100 && kassenCheck[1] > 0) // 1 EURO-Muenzen
	        	{
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 100);
		        	rueckgabebetragInCent -= 100;
		        	kassenCheck[1] -= 1;
	            }
	        while(rueckgabebetragInCent >= 50 && kassenCheck[2] > 0) // 50 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 50);
		        	rueckgabebetragInCent -= 50;
		        	kassenCheck[2] -= 1;
	            }
	        while(rueckgabebetragInCent >= 20 && kassenCheck[3] > 0) // 20 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 20);
		        	rueckgabebetragInCent -= 20;
		        	kassenCheck[3] -= 1;
	            }
	        while(rueckgabebetragInCent >= 10 && kassenCheck[4] > 0) // 10 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 10);
		        	rueckgabebetragInCent -= 10;
		        	kassenCheck[4] -= 1;
	            }
	        while(rueckgabebetragInCent >= 5 && kassenCheck[5] > 0)// 5 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 5);
		        	rueckgabebetragInCent -= 5;
		        	kassenCheck[5] -= 1;
	            }
	        while(rueckgabebetragInCent >= 2 && kassenCheck[6] > 0)// 2 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 2);
		        	rueckgabebetragInCent -= 2;
		        	kassenCheck[6] -= 1;
	            }
	        while(rueckgabebetragInCent >= 1 && kassenCheck[7] > 0)// 1 CENT-Muenzen
	            {
		        	rueckgeld = arrayHinzufuegen(rueckgeld, 1);
		        	rueckgabebetragInCent -= 1;
		        	kassenCheck[7] -= 1;
	            }
	        
	        if (rueckgabebetragInCent > 0) {
	        	System.out.println();
				System.out.println("Nicht genug Rückgeld in der Kasse vorhanden!");
				System.out.println("Fahrscheinausgabe wird abgebrochen!");
				System.out.println("Ausgabe des eingeworfenen Geldes:");
				vonKasseEntfernen(rueckgeldZaehlen(tmpKasse));
				muenzeAusgeben(tmpKasse);
	        } else if ((rohlinge - tmpRohlinge) < 0) {
	        	System.out.println();
				System.out.println("Nicht genug Ticket-Rohlinge vorhanden!");
				if (rohlinge == 0) {
					System.out.println("Keine Ticket-Rohlinge mehr auf Lager.");
				}else if (rohlinge == 1) {
					System.out.println("Nur noch 1 Ticket-Rohling auf Lager.");
				} else {
					System.out.println("Nur noch " + rohlinge + " Ticket-Rohlinge auf Lager.");
				}
				System.out.println("Fahrscheinausgabe wird abgebrochen!");
				System.out.println("Ausgabe des eingeworfenen Geldes:");
				vonKasseEntfernen(rueckgeldZaehlen(tmpKasse));
				muenzeAusgeben(tmpKasse);
	        } else {
				System.out.printf("%nDer Rückgabebetrag in Höhe von %.2f Euro%n", ((double)rueckgabebetragInCentGesamt)/100);
		    	System.out.println("wird in folgenden Münzen ausgezahlt:");
		    	kasse = kassenCheck;
		    	rohlinge -= tmpRohlinge;
				muenzeAusgeben(rueckgeld);
				
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
					"vor Fahrtantritt entwerten zu lassen!\n"+
		            "Wir wünschen Ihnen eine gute Fahrt.");
	        }
		}
	}
	
	private static void normalBetrieb(Scanner tastatur) {
		boolean run = true;
       	
       	while (run) {
       		double bestellung = fahrkartenbestellungErfassen(tastatur);
           	double bezahlung = fahrkartenBezahlen(bestellung, tastatur);
        	rueckgeldAusgeben(bezahlung);
        	System.out.println("====================================================================================================");
        	while (true) {
        		System.out.print("Weiter? (j/n): ");
        		char weiter = tastatur.next().charAt(0);
        		if (weiter == 'j' || weiter == 'y') {
        			break;
        		} else if (weiter == 'n') {
        			System.out.println("Betriebsmodus beendet.");
        			run = false;
        			break;
        		}
        	}      	
       	}
	}
	
	private static void adminBetrieb(Scanner tastatur) {
		boolean run = true;
		
		System.out.println("--Adminbetrieb--");
		while (run) {
			int auswahl = 0;
			
			System.out.print("Funktion auswaehlen:");
       		System.out.printf("%n  %-20s %s","Kasseninhalt zeigen", "(1)");
       		System.out.printf("%n  %-20s %s","Kasse befuellen", "(2)");
       		System.out.printf("%n  %-20s %s","Kasse leeren", "(3)");
       		System.out.printf("%n  %-20s %s","Rohlinge anzeigen", "(4)");
       		System.out.printf("%n  %-20s %s","Rohlinge wechseln", "(5)");
       		System.out.printf("%n  %-20s %s","Zurück", "(6)");
       		System.out.printf("%n%n");
       		System.out.print("Ihre Wahl: ");

    		try {
    			auswahl = tastatur.nextInt();
       		} catch (Exception e) {
       			tastatur.next();
       		}
    		
    		switch (auswahl) {
	    		case 1:
					kasseAnzeigen();
					break;
	    		case 2:
    				kasseBefuellen();
    				break;
	    		case 3:
    				kasseLeeren();
    				break;
	    		case 4:
    				rohlingeAnzeigen();
    				break;
	    		case 5:
    				rohlingeWechseln();
    				break;
	    		case 6:
    				run = false;
    				break;
	    		default:
	    			System.out.println("Bitte unter den gegebenen Funktionen auswählen!");
	    			break;
    		}
		}
	}
	
	public static void automatStarten() {
		Scanner tastatur = new Scanner(System.in);
       	boolean run = true;
       	
       	while(run) {
       		int betriebsmodus = 0;
       		
       		System.out.print("Betriebsmodus auswählen:");
       		System.out.printf("%n  %-20s %s", "Normalbetrieb", "(1)");
       		System.out.printf("%n  %-20s %s", "Adminbetrieb", "(2)");
       		System.out.printf("%n  %-20s %s", "Programm beenden", "(3)");
       		System.out.printf("%n%n");
       		System.out.print("Ihre Wahl: ");
       		
       		try {
       			betriebsmodus = tastatur.nextInt();
       		} catch (Exception e) {
       			tastatur.next();
       		}
    		    		
    		switch (betriebsmodus) {
    		case 1:
    			normalBetrieb(tastatur);
    			break;
    		case 2:
    			adminBetrieb(tastatur);
    			break;
    		case 3:
    			System.out.println("--Programm beendet--");
    			run = false;
    			break;
    		default:
    			System.out.println("Bitte unter den gegebenen Punkten auswählen!");
    			break;
    		}
       	}
	}
		
    public static void main(String[] args)
    {
       	automatStarten();
    } 
	
}