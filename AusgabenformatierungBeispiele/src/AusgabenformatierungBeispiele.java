
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {

/*		System.out.printf("%s%n", "123456789");
		System.out.printf("|%s|%n", "123456789");
		System.out.printf("|%20s|%n", "123456789");
		System.out.printf("|%-20s|%n", "123456789");
*/
		//Aufgabe 1 - Kreiszeichnen
		System.out.printf("%4s%n%-5s%s%n%-5s%s%n%4s", "**", "*", "*", "*", "*", "**");
		
		System.out.println();
		System.out.println();
		
		//Aufgabe 2 - Fakultätberechnen
		System.out.printf("%-5s=%-19s=%4s%n", "0!", "", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "1!", " 1", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "2!", " 1 * 2", "2");
		System.out.printf("%-5s=%-19s=%4s%n", "3!", " 1 * 2 * 3", "6");
		System.out.printf("%-5s=%-19s=%4s%n", "4!", " 1 * 2 * 3 * 4", "24");
		System.out.printf("%-5s=%-19s=%4s%n", "5!", " 1 * 2 * 3 * 4 * 5", "120");
		
		System.out.println();
		System.out.println();
		
		//Aufgabe 3 - Temperaturtabelle
		System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
		System.out.printf("%22s%n", "-------------------------");
		System.out.printf("%+-12d|%+10.2f%n", -20, -28.8889);
		System.out.printf("%+-12d|%+10.2f%n", -10, -23.3333);
		System.out.printf("%+-12d|%+10.2f%n", 0, -27.7778);
		System.out.printf("%+-12d|%+10.2f%n", 20, -6.6667);
		System.out.printf("%+-12d|%+10.2f%n", 30, -1.1111);
	}

}
